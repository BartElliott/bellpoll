# encoding: utf-8

''' Only allow python3--it IS POSSIBLE to have python2 support if I 
assign the download function of the save name a different value based on 
python 2 or 3.
'''
import sys
if sys.version_info[0] != 3:
    sys.stdout.write('No support for python 2, only python 3, sorry!')
    exit()


import urllib.request
from os import getcwd
from os.path import join, dirname
from time import localtime
from constants import REGULAR_GAME, BOWL_GAME, EAST_WEST_GAME

# For convenience, assign the urllib library function to another name.
save_url_to_file = urllib.request.urlretrieve


'''

example: http://scores.espn.go.com/ncf/scoreboard?confId=80&
seasonYear=2003&seasonType=3&weekNumber=10

confId = 80 for FBS

seasonYear= <from 2003 - 2014>

seasonType= 2 for regular season, 3 for bowl game, 
4 for all-star game.
(Is 1 reserved for preseason, is that why there isn't a 1? Maybe.)

weekNumber: Weeks: 1 - 15 or 1 - 16
and bowls and all-star (week 17 for bowl game, all-star games are week 1)
- any week more than the last week is redirected to the final week of 
the season.

'''


def main():
    # Setup dictionary of year to weekNums (including bowl week, NOT E/W Game)

    #TODO: Switch this scheme to instead fetch page after page until 
    # a page doesn't have any new scores (instead of knowing week nums).
    #TODO: Depend on time.localtime().tm_year for current year.

    yearsBefore2008 = {2003: 17, 2004: 17, 2005: 16,
                       2006: 17, 2007: 17}
    years2008AndAfter = {2008: 17, 2009: 16, 2010: 16,
                         2011: 16, 2012: 16, 2013: 17}
    
    for year, nWeeks in yearsBefore2008.items():
        download_year(year, nWeeks, False)
        
    for year, nWeeks in years2008AndAfter.items():
        download_year(year, nWeeks, True)
        
    
def download_helper(year, weekNum, seasonType):
    url = 'http://scores.espn.go.com/ncf/scoreboard?confId=80&seasonYear=' + \
    str(year) + '&seasonType=' + str(seasonType) + '&weekNumber=' + str(weekNum)
    
    strWeekNum = str(weekNum)
    if (weekNum < 10):
        strWeekNum = '0' + strWeekNum
    fileName = join(dirname(getcwd()), 'res', str(year) + '-' + strWeekNum)
    save_url_to_file(url, fileName)
    print('Saved file: ' + fileName)

    
def download_year(year, bowlWeekNum, hasEastWest):
    for weekNum in range(1, bowlWeekNum):
        download_helper(year, weekNum, REGULAR_GAME)
    
    download_helper(year, bowlWeekNum, BOWL_GAME)
    
    if (hasEastWest):
        download_helper(year, bowlWeekNum + 1, EAST_WEST_GAME)
    

if __name__ == '__main__':
    main()
